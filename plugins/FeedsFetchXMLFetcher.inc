<?php

/**
 * @file
 * Home of the FeedsFetchXMLFetcher and related classes.
 */

/**
 * Definition of the import batch object created on the fetching stage by
 * FeedsSoapFetcher.
 */
class FeedsFetchXMLBatch extends FeedsFetcherResult {
  protected $config;
  protected $client;

  /**
   * Constructor.
   */
  public function __construct($config) {
    $this->config = $config;

    $options = array('trace' => TRUE);
    if ($this->config['username']) {
      $options['login'] = $this->config['username'];
      $options['password'] = $this->config['password'];
      $options['organization'] = $this->config['organization'];
    }

    watchdog('feeds_fetchxml',"Creating connector",array(),WATCHDOG_DEBUG);
    $this->client = new DynamicsCRM2011_Connector($this->config['endpoint'],$options['organization'] , $options['login'], $options['password']) ;
      
    parent::__construct();
  }

  /**
   * Implementation of FeedsImportBatch::getRaw().
   */
  public function getRaw() {
    $args = array();
    watchdog('feeds_fetchxml',"Getting response",array(),WATCHDOG_DEBUG);
    $response = $this->client->retrieveMultipleRaw($this->config['fetchxml']);
    return $response;
  }
}

/**
 * Fetches data via php-dynamics-crm-2011
 */
class FeedsFetchXMLFetcher extends FeedsFetcher {

  /**
   * Implementation of FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    // Note: we fetch data using the Feed config instead of using the source
    // import form. The reason for this, is that does not make sense allowing
    // the user to switch the SOAP endpoint, when it requires a much more
    // complex configuration.
    return new FeedsFetchXMLBatch($this->config);
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'endpoint' => '',
      'username' => '',
      'password' => '',
      'organization' => '',
      'fetchxml' => '' 
    );
  }
  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form['endpoint'] = array(
      '#type' => 'textfield',
      '#title' => t('SOAP server endpoint URL'),
      '#size' => 60,
      '#maxlength' => 256,
      '#description' => t('Enter the absolute endpoint URL of Dynamics CRM 2011 (or compatible). it have to use claim authentication to this feeds fether to work.'),
      '#default_value' => $this->config['endpoint'],
      '#required' => TRUE
    );

    $form['username'] = array(
      '#type' => 'textfield',
      '#title' => 'Username',
      '#default_value' => $this->config['username'],
      '#required' => TRUE
    );

    $form['password'] = array(
      '#type' => 'textfield',
      '#title' => 'Password',
      '#default_value' => $this->config['password'],
      '#required' => TRUE
    );
    $form['organization'] = array(
      '#type' => 'textfield',
      '#title' => 'Organisation',
      '#default_value' => $this->config['organization'],
      '#required' => TRUE
    );

    $form['fetchxml'] = array(
      '#type' => 'textarea',
      '#title' => t('FetchXML for query'),
      '#cols' => 60,
      '#rows' => 10,
      '#description' => t('Use Dynamics CRM 2011 advanced search to create FetchXML for query.'),
      '#default_value' => $this->config['fetchxml'],
    );

    return $form;
  }

  /**
   * Expose source form.
  public function sourceForm($source_config) {
    $form = array();

    // HACK: the hidden field is required to allow Feeds to process the import
    $form['source'] = array('#type' => 'hidden', '#value' => 'hack_to_allow_import');

    $form['info'] = array(
      '#type' => 'item',
      '#value' => t('Will import from %function at %endpoint', array('%function' => $this->config['function'], '%endpoint' => $this->config['endpoint'])),
    );
    return $form;
  }

   */

 /**
   * Override parent::sourceForm().
   */
  public function sourceForm($source_config) {
    $tokens = array(
      '!edit_link' => l(t('Edit Feed'), 'admin/structure/feeds/edit/' . $this->id),
    );

    $form_state = array();
    //$form = $this->configForm($form_state);
    $form['addendum'] = array(
      '#type' => 'markup',
      '#markup' => t('This import is configured at !edit_link.', $tokens),
    );

    return $form;
  }
}
