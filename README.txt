Feeds FetchXML Fetcher


Quick Start for Dynamics CRM 2011

Source system needs to be configured to use claim based authentication and ssl.

Before you can enable this module you have to download php-dynamics-crm-2011-classes from
http://code.google.com/p/php-dynamics-crm-2011/
And extract class files to feeds_fetchxml/classes . 

If it's ok to include GNU Lesser GPL lisensed files in drupal git-repo let me know and I'll
include those libraryfiles.

Than you can enable Feeds FetchXML

How to use Feeds FetchXML:

1. Create new Feeds Importer and specify  
   SOAP server endpoint URL 
   https://[sourceserver]/XRMServices/2011/Discovery.svc
   Fill in Username, password and organization name

   For FetchXML query use Dynamics webinterface and it's advanced search tool to build up query. 
   Remember to add Entity GUID attribute to query.

2. Create Processor mappings for example for Node Processor create
   xpath mapping for GUID, Title and Body


3. Select Feeds XPATH parser settings

   For Context specify something like:
   "/s:Envelope/s:Body/RetrieveMultipleResponse/RetrieveMultipleResult/b:Entities/b:Entity"

   For GUID attributei something like, replacing "custom_guid" with CRM attribute name conteinin Entity GUID:
   "b:Attributes/b:KeyValuePairOfstringanyType[c:key='custom_guid']/c:value"

   For rest of attributes use same syntaxt as:
   "b:Attributes/b:KeyValuePairOfstringanyType[c:key='title']/c:value"
   "b:Attributes/b:KeyValuePairOfstringanyType[c:key='description']/c:value"

   When debugging you xPath statements it's usefull set "Allow source configuration override" to true and use
   debug setting to write out context, guid .. variable values.

4. Use Node prosessor setting to specify mappinings between xPath Parser and nodei prosessor fields.
   And specify node type what you like use.


